package io.beskrovnykh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebsocketStompDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebsocketStompDemoApplication.class, args);
    }

}
