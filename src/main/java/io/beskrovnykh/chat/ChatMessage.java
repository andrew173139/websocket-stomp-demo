package io.beskrovnykh.chat;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level= AccessLevel.PRIVATE)
public class ChatMessage {
    public enum MessageType {
        CHAT,
        JOIN,
        LEAVE,
        TEST
    }
    MessageType type;
    String content;
    String sender;
}