package io.beskrovnykh.chat;

import java.util.*;

public class ChatRepository {

    public static ChatRepository instance = new ChatRepository();

    private final Map<String, ChatMessage> messages = new LinkedHashMap<>();

    public List<ChatMessage> getMessages() {
        return new ArrayList<>(messages.values());
    }

    public void addMessage(ChatMessage chatMessage) {
        messages.put(UUID.randomUUID().toString(), chatMessage);
    }
}