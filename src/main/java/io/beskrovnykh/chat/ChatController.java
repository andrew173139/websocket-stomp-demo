package io.beskrovnykh.chat;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ChatController {

    @MessageMapping("/chat/sendMessage")
    @SendTo("/topic/public")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {
        ChatRepository.instance.addMessage(chatMessage);
        return chatMessage;
    }

    @SubscribeMapping("/chat/subscribe")
    public List<ChatMessage> subscribe() {
        return ChatRepository.instance.getMessages();
    }

    @MessageMapping("/chat/addUser")
    @SendTo("/topic/public")
    public ChatMessage addUser(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        // add username to web socket session attributes
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        ChatRepository.instance.addMessage(chatMessage);
        return chatMessage;
    }


}
